#!/bin/sh

rm -rf db.sqlite3
find . -path "src/*/migrations/*.py" -not -name "__init__.py" -not -name "0001_initial.*" -delete
find . -path "src/*/migrations/*.pyc"  -delete

src/manage.py makemigrations core && src/manage.py migrate core