#!/bin/sh

directory=medux/core/static/core
VERSION=5.0.2


die() {
  echo $1
  exit 1
}

if [ "$DEBUG" = "False" ]; then
  MIN=".min"
else
  unset MIN
fi

download() {
  # $1: url to download
  # $2: [css|js]

  echo -n "Fetching $1... "
  wget -q $1 -O ${directory}/$2/bootstrap.$2 \
    && echo "ok." \
    || echo "Error fetching $2."
}

[ -d ${directory} ] || die "Please call this script from the root medux directory."

download "https://cdn.jsdelivr.net/npm/bootstrap@${VERSION}/dist/css/bootstrap${MIN}.css" css

download "https://cdn.jsdelivr.net/npm/bootstrap@${VERSION}/dist/js/bootstrap.bundle${MIN}.js" js
