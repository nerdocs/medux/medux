#
# MedUX - Open Source Electronical Medical Record
# Copyright (c) 2022  Christian González
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#!/bin/sh

for f in htmx.js htmx.min.js; do
  echo "downloading ${f} from unpkg.com"
  wget https://unpkg.com/htmx.org@latest/dist/${f} -o  medux/common/static/common/js/htmx/${f} -q
done

for ext in debug ws; do
  echo "downloading ext/${ext} from unpkg.com"
  wget https://unpkg.com/htmx.org@latest/dist/ext/${ext}.js -o  medux/common/static/common/js/htmx/ext/${ext}.js -q
done

