#!/bin/sh

if [ "${1}" = "" ]; then
  cat << EOF
Creates a class diagram of your app.

Usage: ${0} <app>

where <app> is an installed Django app in your project.
EOF
  exit 1
fi

python src/manage.py graph_models -g --pygraphviz -o "${1}.png" "${1}"
