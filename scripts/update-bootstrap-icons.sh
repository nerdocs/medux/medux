#!/bin/sh

directory=medux/core/static/css/

die() {
  echo $1
  exit 1
}

[ -d ${directory} ] || die "Please call this script from the root medux directory."
wget https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css -O ${directory}bootstrap-icons.css -q
