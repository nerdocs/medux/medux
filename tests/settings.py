"""
MedUX testing settings.
"""
from medux.tests.settings import *  # noqa

# use a name suitable for MedUX.
DATABASES["default"]["NAME"] = "medux-test.db"  # noqa
