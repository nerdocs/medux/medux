from inspect import iscoroutinefunction
from asgiref.sync import markcoroutinefunction, sync_to_async
from django.http import HttpRequest


@sync_to_async
def get_user_from_request(request: HttpRequest):
    return request.user if request.user else None


class EmployeeMiddleware:
    """Adds .employee attribute to request objects.

    If current user is no employee, it is set to None.
    """

    async_capable = False
    sync_capable = True

    def __init__(self, get_response):
        self.get_response = get_response
        if iscoroutinefunction(self.get_response):
            markcoroutinefunction(self)

    def __call__(self, request):
        # Get the currently logged-in user
        # FIXME: this must be made async capable!
        # user = await get_user_from_request(request)
        # user = await sync_to_async(lambda: request.user)()
        user = request.user
        request.employee = None
        # user = await get_user(request)
        # Check if the user is authenticated and is an instance of the Employee model
        if user.is_authenticated and hasattr(user, "employee"):
            request.employee = user.employee
        else:
            request.employee = None

        response = self.get_response(request)
        return response
