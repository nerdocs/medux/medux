from crispy_forms.helper import FormHelper
from crispy_forms.layout import Row, Column, ButtonHolder, Submit, Layout
from django import forms
from django.urls import reverse

from .models import Employee


class TenantEmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = [
            "username",
            "title",
            "first_name",
            "last_name",
            "email",
            "is_active",
            "color",
            "avatar",
        ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.helper = FormHelper()
        if self.instance.pk:
            self.fields["username"].disabled = True
            self.helper.form_action = reverse(
                "adm:employee:update", kwargs={"pk": self.instance.pk}
            )
        else:
            self.helper.form_action = reverse("adm:employee:add")
        self.helper.layout = Layout(
            Row(Column("username"), Column("is_active", css_class="col-12 col-md-3")),
            Row(
                Column("title", css_class="col-12 col-md-2"),
                Column("first_name"),
                Column("last_name"),
            ),
            "email",
            "color",
            "avatar",
            ButtonHolder(
                Submit("save", "Save"),
            ),
        )
