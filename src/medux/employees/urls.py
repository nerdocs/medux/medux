from django.urls import path
from . import views
from medux.common.api.interfaces import IAdministrationURL

app_name = "employees"

urlpatterns = [
    # WorkingContract
    path(
        "<int:pk>/contracts/current/",
        views.WorkingContractWidgetView.as_view(),
        name="workingcontract-current",
    ),
    path(
        "<int:employee_pk>/contracts/<int:pk>/",
        views.WorkingContractUpdateView.as_view(),
        name="workingcontract-change",
    ),
    path(
        "<int:employee_pk>/contracts/add/",
        views.WorkingContractCreateView.as_view(),
        name="workingcontract-create",
    ),
    path(
        "<int:pk>/contract/<int:contract_pk>/salary/",
        views.SalaryView.as_view(),
        name="salary",
    ),
]


class EmployeeAdministrationURL(IAdministrationURL):
    namespace = "employee"
    urlpatterns = [
        path(
            "",
            views.TenantEmployeeListView.as_view(),
            name="list",
        ),
        # path(
        #     "<int:pk>/",
        #     views.TenantEmployeeDetailView.as_view(),
        #     name="detail",
        # ),
        path(
            "<int:pk>/change/",
            views.TenantEmployeeUpdateView.as_view(),
            name="update",
        ),
        path(
            "add/",
            views.TenantEmployeeCreateView.as_view(),
            name="add",
        ),
    ]
