from datetime import date

import pytest
from medux.common.models import Tenant, SexChoices
from medux.employees.models import (
    Classification,
    Application,
    WorkSchedule,
    Employee,
    WorkingContract,
)


@pytest.fixture
def tenant_a():
    return Tenant.objects.create(
        last_name="Picard",
        first_name="James",
        address="Earth",
        sex=SexChoices.MALE,
    )


@pytest.fixture
def tenant_b():
    return Tenant.objects.create(
        last_name="Worf",
        first_name="Gagor",
        address="Qo'noS",
        sex=SexChoices.MALE,
    )


@pytest.fixture
def employee_a1(tenant_a):
    return Employee.objects.create_user(
        username="employee_a1", password="password", tenant=tenant_a
    )


@pytest.fixture
def employee_b1(tenant_b):
    return Employee.objects.create_user(username="employee_b1", tenant=tenant_b)


@pytest.fixture
def classification_foo():
    return Classification.objects.create(name="foo")


@pytest.fixture
def application_med_assistant():
    return Application.objects.create(name="Medical assistant")


@pytest.fixture
def workingcontract_march_2019(
    employee_a1, classification_foo, application_med_assistant
):
    """returns a WorkingContract object, without saving it."""
    return WorkingContract(
        employee=employee_a1,
        tenant=employee_a1.tenant,
        start_date=date(2019, 3, 1),
        end_date=date(2019, 3, 31),
        employment_date=date(2019, 3, 1),
        classification=classification_foo,
        intended_application=application_med_assistant,
        initial_salary=0,
    )


@pytest.fixture
def workschedule_march_2019(employee_a1, workingcontract_march_2019):
    workingcontract_march_2019.save()
    return WorkSchedule.objects.create(
        employee=employee_a1,
        working_contract=workingcontract_march_2019,
        start_date=date(year=2019, month=3, day=1),
        end_date=date(year=2019, month=3, day=31),
    )
