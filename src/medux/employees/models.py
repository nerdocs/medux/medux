import calendar
import decimal
import typing

from datetime import timedelta, date
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Sum, F, Q
from django.utils import timezone
from django.utils.formats import date_format, time_format
from django.utils.translation import gettext_lazy as _
from djmoney.models.fields import MoneyField

from medux.common.constants import DaysOfWeek
from medux.common.models import BaseModel, Tenant, CreatedModifiedModel

User = get_user_model()


class Classification(models.Model):
    """The classification in an employment context.

    This could be "doctor", "nurse", etc.
    """

    class Meta:
        verbose_name = _("Classification")
        verbose_name_plural = _("Classifications")

    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Application(models.Model):
    """The "usage field" of an employee.

    Could be "medical assistent"
    """

    class Meta:
        verbose_name = _("Application")
        verbose_name_plural = _("Applications")

    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class WorkingContract(BaseModel):
    """A contract a user has with his employer.

    Working hours, salary, and other details should be fixed within a contract,
    for a given period.
    """

    class Meta:
        verbose_name = _("Working contract")
        verbose_name_plural = _("Working contracts")

    employee = models.ForeignKey(
        "Employee", on_delete=models.CASCADE, related_name="working_contracts"
    )
    tenant = models.ForeignKey(Tenant, on_delete=models.CASCADE)
    start_date = models.DateField(
        _("Start date"), help_text=_("Start date of this contract")
    )
    end_date = models.DateField(
        _("End date"), help_text=_("End date of this contract"), blank=True, null=True
    )
    employment_date = models.DateField(
        _("Employment date"),
        help_text=_("First date of employment at this working place"),
        blank=True,
    )
    notice_period_weeks = models.PositiveIntegerField(
        _("Notice period in weeks"), blank=True, null=True
    )
    notice_period_months = models.PositiveIntegerField(
        _("Notice period in months"), blank=True, null=True
    )
    # location = models.ForeignKey(Practice)
    classification = models.ForeignKey(Classification, on_delete=models.PROTECT)
    intended_application = models.ForeignKey(Application, on_delete=models.PROTECT)
    initial_salary = MoneyField(max_digits=14, decimal_places=2, default_currency="EUR")
    salary_notice = models.TextField(
        _("Additional notice for salary"), blank=True, null=True
    )
    holiday_year_is_calendar_year = models.BooleanField(
        default=False,
        help_text=_(
            "If activated, the holiday year is the same as a calendar year, "
            "else it starts at the employment date."
        ),
    )

    def __str__(self):
        # return f"{self._meta.verbose_name}.."
        end_date = (
            f" - {date_format(self.end_date, 'SHORT_DATE_FORMAT')}"
            if self.end_date
            else ""
        )
        return (
            f"{self.employee}: {date_format(self.start_date, 'SHORT_DATE_FORMAT')}"
            f"{end_date}"
        )

    def clean(self):
        """Set employment_date if empty"""
        if not self.employment_date:
            self.employment_date = self.start_date

    def duration(self) -> timedelta:
        """The duration of that contract in days.

        If contract has no end date, count until today (including the day today)
        """
        if self.end_date:
            end_date = self.end_date
        else:
            end_date = timezone.now().date() + timedelta(days=1)  # =tomorrow 0:00
        return end_date - self.start_date

    def days_in_year(self, year) -> timedelta:
        """Returns timedelta from Jan 1st (or contract start) until Dec 31th (or
        contract end) for given year."""

        year_start = date(year, 1, 1)
        year_end = date(year, 12, 31)
        # start date is Jan 1st or contract start (if this year)
        start = year_start if self.start_date < year_start else self.start_date
        if self.end_date:
            # end date is Dec 31th or contract end (if this year)
            end = year_end if self.end_date > year_end else self.end_date
        else:
            # no contract end: take Dec 31st
            end = year_end

        # add one day, to include the last day.
        end += timedelta(days=1)

        # if contract doesn't touch given year at all, return 0
        if end.year < year or start.year > year:
            return timedelta(0)

        delta = end - start
        if delta.days < 0:
            delta = timedelta(0)
        return delta

    def is_active(self, day: date = None):
        if not day:
            day = timezone.now().date()
        return self.employee.active_working_contract(day) is not None

    def create_subsequent_contract(self, new_start_date: date) -> "WorkingContract":
        """Closes this contract one day before new_start_date, and returns an (unsaved)
        copy contract subsequently following the old one."""
        self.end_date = new_start_date - timedelta(days=1)
        self.save()
        return WorkingContract(
            employee=self.employee,
            tenant=self.tenant,
            start_date=new_start_date,
            employment_date=self.employment_date,  # stays the same
            notice_period_weeks=self.notice_period_weeks,
            notice_period_months=self.notice_period_months,
            classification=self.classification,
            intended_application=self.intended_application,
            initial_salary=self.initial_salary,
            salary_notice=self.salary_notice,
        )


class WorkSchedule(CreatedModifiedModel):
    """A set of working hour ranges, for one user / one week, in a valid time range.

    E.g. Mon 8-12, Tue 8-12, Wed-Fr 9-11

    [WorkingTimeRange] has a ForeignKey to this class, so you can access them using
    `self.working_hours`.

    There can be more WorkSchedules for one WorkingContract: Within a contract, you just
    can change the working hours, hence changing the WorkSchedule.
    """

    class Meta:
        verbose_name = _("Work schedule")
        verbose_name_plural = _("Work schedules")

    employee = models.ForeignKey(
        "Employee", on_delete=models.CASCADE, related_name="work_schedules"
    )
    start_date = models.DateField(_("Start date"), default=timezone.localdate)
    end_date = models.DateField(_("End date"), blank=True, null=True)
    working_contract = models.ForeignKey(
        WorkingContract,
        on_delete=models.CASCADE,
        related_name="work_schedules",
        null=True,  # FIXME - this must not be there.
    )

    def total_working_time(self) -> timedelta:
        total_hours = self.working_hours.all().aggregate(
            total=Sum(F("end_time") - F("start_time"))
        )
        return total_hours["total"] or timedelta(0)

    def total_working_time_h(self) -> decimal.Decimal:
        t = self.total_working_time()
        return decimal.Decimal(t.days * 24 * 60 * 60 + t.seconds) / (60 * 60)

    def work_days_per_week(self) -> int:
        """Returns the number of days per week the employee is working during this
        period.

        This takes into account that more than one WTR per day may be possible.
        """
        # TODO: maybe optimize this function by using one DISTINCT SQL query
        #       (=aggregate/sum in Django)
        weekdays = set()
        for wtr in self.working_hours.all():
            weekdays.add(wtr.weekday)
        return len(weekdays)

    def __str__(self) -> str:
        active = (
            " [" + _("active") + "]"
            if not self.end_date
            else date_format(self.end_date, "SHORT_DATE_FORMAT")
        )
        return (
            f"{self.employee}: {date_format(self.start_date,'SHORT_DATE_FORMAT')}"
            f" - {active} "
            f"({self.total_working_time_h()})"
        )

    @classmethod
    def get_active(cls, employee) -> typing.Union["WorkSchedule", None]:
        """Returns "active" work schedule, which is the one that has no end date.

        there *has* to be only one WorkSchedule that is "active", meaning:
         * start date <= today
         * no end date, or
         * end date in future
        """
        try:
            return cls.objects.get(
                Q(employee=employee),
                Q(start_date__lte=timezone.now()),
                Q(end_date__isnull=True) | Q(end_date__gte=timezone.now()),
            )
        except cls.DoesNotExist:
            return None

    def clean(self):
        error_list = []
        # check if there is another schedule that starts *after* self
        if not self.end_date:
            active = self.get_active(self.employee)

            if active and active != self:
                if active.start_date >= self.start_date:
                    raise ValidationError(
                        {
                            "start_date": _(
                                "There is another active work schedule ({active})"
                                " that begins after this one."
                            ).format(active=active)
                        }
                    )
            # is there another WorkSchedule without end date?
            overlapping_list = WorkSchedule.objects.filter(
                employee=self.employee, end_date__isnull=True
            ).exclude(pk=self.pk)
        else:
            # is there another WorkSchedule that overlaps or has no end date?
            overlapping_list = WorkSchedule.objects.filter(
                Q(employee=self.employee),
                Q(start_date__lte=self.end_date),
                Q(end_date__gte=self.start_date) | Q(end_date__isnull=True),
            ).exclude(pk=self.pk)

        for overlap in overlapping_list:
            error_list.append(
                ValidationError(
                    _(
                        "Work schedule overlaps with another schedule: {schedule}"
                    ).format(schedule=overlap)
                )
            )
        # check if there is an active working contract containing this WS (timely)
        if self.working_contract:
            contract = self.working_contract

            if (
                self.start_date < self.working_contract.start_date
                or contract.end_date and self.start_date > contract.end_date
            ):
                # don't go further, this is a fatal error.
                raise ValidationError(
                    _(
                        "Working contract starts outside of ({wcstart})"
                        " working schedule: {wsstart}."
                    ).format(
                        wcstart=contract.start_date,
                        wsstart=self.start_date,
                    )
                )

            # truncate WS end date at contract's end date
            if contract.end_date and (
                not self.end_date or self.end_date > contract.end_date
            ):
                self.end_date = contract.end_date
        else:
            # no contract given, try to find one.
            if self.end_date:
                contracts = WorkingContract.objects.filter(
                    Q(end_date__isnull=True) | Q(end_date__gte=self.end_date),
                    employee=self.employee,
                    start_date__lte=self.start_date,
                )
            else:
                # this WS has no end date. contract must have none neither.
                # TODO evtl. decide to crop WS at WC.end_date here
                contracts = WorkingContract.objects.filter(
                    employee=self.employee,
                    start_date__lte=self.start_date,
                    end_date__isnull=True,
                )
            if not contracts:
                # no matching contracts found
                error_list.append(
                    ValidationError(
                        _(
                            "No working contract found for {employee} "
                            "within {start}-{end}. Please create one before "
                            "creating a work schedule..."
                        ).format(
                            employee=self.employee,
                            start=self.start_date,
                            end=self.end_date,
                        )
                    )
                )

        if error_list:
            raise ValidationError(error_list)

    def save(self, update_last_active=True, **kwargs):
        """Model save method with option to update last active record's end date"""
        if update_last_active and not self.end_date:
            # end last WorkSchedule with day before this one starts.
            last_active = self.get_active(self.employee)
            if last_active and self is not last_active:
                last_active.end_date = self.start_date - timedelta(days=1)
                last_active.save(update_last_active=False)
        super().save(**kwargs)

    @classmethod
    def get_matching(cls, employee, day: date) -> typing.Optional["WorkSchedule"]:
        # Retrieve the WorkSchedule instance that matches the given date
        try:
            # Filter WorkSchedule instances where the_date is between start_date and
            # end_date, or where start_date is before or equal to the_date and end_date
            # is null
            matching_instance = cls.objects.get(
                Q(employee=employee),
                Q(start_date__lte=day)
                & (Q(end_date__gte=day) | Q(end_date__isnull=True)),
            )
        except cls.DoesNotExist:
            matching_instance = None

        return matching_instance

    def create_subsequent_schedule(self, new_start_date: date) -> "WorkSchedule":
        """Ends this WorkSchedule one day before new_start_date, and returns an
        (unsaved) copy subsequently following the old one."""
        self.end_date = new_start_date - timedelta(days=1)
        self.save()
        return WorkSchedule(
            employee=self.employee,
            working_contract=self.working_contract,
            start_date=self.new_start_date,
        )


class Employee(User):
    """Submodel for user to encapsulate employment functions.

    There can be users that are no employees, like admin, or system users etc.
    """

    def job_title(self):
        contract = self.active_working_contract(timezone.now().date())
        if contract and contract.intended_application:
            return contract.intended_application
        else:
            return super().job_title()

    def active_working_contract(self, reference_date) -> WorkingContract | None:
        """Returns the working contract that is valid for a given day, or None."""

        if not self.working_contracts.count():
            # e.g. admin user
            return None

        first = self.working_contracts.filter(
            Q(start_date__lte=reference_date),
            Q(end_date__isnull=True) | Q(end_date__gte=reference_date),
        ).first()
        if not first:
            return None

        return first

    def active_work_schedule(self, day: date) -> WorkSchedule | None:
        """Returns the work schedule that is valid for given day, or None."""

        schedule = self.work_schedules.filter(
            Q(start_date__lte=day), Q(end_date__isnull=True) | Q(end_date__gte=day)
        )
        if not schedule.exists():
            return None

        # If multiple work schedules were found, return the one with the earliest start
        # date overlapping_schedules = overlapping_schedules.order_by('start_date')
        # work_schedule = overlapping_schedules.first()

        # If the found work schedule ends before the given day, it is no longer valid
        # if work_schedule.end_date and work_schedule.end_date < day:
        #     return None

        return schedule.first()


class WorkingTimeRange(models.Model):
    """A working hours range blueprint

    This is meant generically, e.g. for each Monday 8:00-15:00.
    There can be more than one WorkingTimeRange objects for one weekday, e.g.
    an employee could work on Tuesdays from 8-11 and 16-19.

    For real "worked" hours, use [TimeEntry] from the timetracker plugin.
    """

    class Meta:
        verbose_name = _("Working time range")
        verbose_name_plural = _("Working time ranges")

    weekday = models.PositiveSmallIntegerField(
        _("Week day"), choices=DaysOfWeek.choices
    )
    start_time = models.TimeField(_("Start time"))
    end_time = models.TimeField(_("End time"))
    work_schedule = models.ForeignKey(
        WorkSchedule, on_delete=models.CASCADE, related_name="working_hours"
    )

    def __str__(self):
        return (
            f"{_(calendar.day_abbr[self.weekday])} "
            f"{time_format(self.start_time)}-"
            f"{time_format(self.end_time)}"
        )

    def duration(self) -> timedelta:
        return timedelta(
            hours=self.end_time.hour - self.start_time.hour,
            minutes=self.end_time.minute - self.start_time.minute,
        )

    def duration_h(self) -> float:
        return self.duration().seconds / 60 / 60

    def weekday_str(self) -> str:
        return _(calendar.day_name[self.weekday])
