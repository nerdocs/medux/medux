from django.views.generic import DetailView, CreateView, ListView

from medux.core.models import Patient


class PatientListView(ListView):
    model = Patient
    permission_required = "can list patients"


class PatientFileView(DetailView):
    # template_name = "patient_detail.html"
    permission_required = "can view patients"
    model = Patient

    # def get_context_data(self, *args, **kwargs):
    #     context = super().get_context_data(*args, **kwargs)
    #     return context


class NewPatientView(CreateView):
    model = Patient
    permission_required = "can add patients"
    fields = ["names"]
