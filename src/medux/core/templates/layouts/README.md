# Layouts

This directory includes files that can be extended in plugins.

* base.html - base for all "normal" pages
* empty.html - base for empty pages, as login page or certain error pagey may need.
* modal.html - base for all modal dialogs.