from gdaps.api import Interface

from medux.common.api.interfaces import IHtmxComponentMixin


@Interface
class IGlobalJavascript:
    """Interface for adding Js file to a global scope.

    The given file will be loaded in the global context and is then available to
    all other plugins too, in every loaded page.
    Be aware just to add Js code that is small and fast, to not blow up the application
    as whole.

    You have to specify the (relative to static dir) file path of the Js script in the
    `file` attribute.
    """

    __service__ = True

    file: str = ""
    """The (relative to static dir) file path of the Js script"""


@Interface
class ICommand:
    """A CommandLine command that executes a defined function on a shortcut.

    A written command could be ``m dicl 75`` what means the "m" command with could be
    parsed as "medication" command with a search for "Diclofenac 75mg Tbl"."""

    shortcut: str = None
    description: str = None

    def execute(self, *args, **kwargs):
        raise NotImplementedError


@Interface
class IDashboardSection(IHtmxComponentMixin):
    form_kwargs_request = True

    def has_permission(self):
        return self.request.user.is_authenticated
