# This file contains base models for master data used in MedUX. They can be provided
# using packages.

from uuid import UUID, uuid4

from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Manager, Model
from django.utils.translation import gettext_lazy as _

from medux.common.models import BaseModel, Tenant
from medux.core.fields import MarkdownField


class PackageDataManager(Manager):
    """A manager that adds useful functionality in a multi tenant environment.

    It returns requested tenant's objects, and if not found, fetches the vendor object
    as fallback. Adds a convenience `create_as_vendor()` method for vendors, and forces
    a "tenant" attribute for the `create()` method.
    """

    def filter(self, *args, **kwargs):
        if "tenant" in kwargs:
            try:
                return super().filter(*args, **kwargs)
            except Model.DoesNotExist:
                # remove tenant from filter options, return vendor object as fallback.
                del kwargs["tenant"]
                return super().filter(*args, tenant=None, **kwargs)
        return super().filter(*args, **kwargs)

    def create(self, *args, **kwargs):
        """Checks if tenant is given in kwargs. If not, raises a ValidationError."""

        # if undocumented __vendor parameter is given, create as vendor (without tenant)

        if "__vendor" in kwargs:
            if "tenant" in kwargs:
                raise ValidationError(
                    _(
                        f"A tenant cannot be provided when saving {self} "
                        "as a vendor object."
                    )
                )
            del kwargs["__vendor"]
        else:
            if "tenant" not in kwargs:
                raise ValidationError(
                    _(
                        "The 'tenant' attribute is not is not given. "
                        "If you want to create vendor data, use create_as_vendor() "
                        "instead."
                    )
                )
        return super().create(*args, **kwargs)

    def create_as_vendor(self, *args, **kwargs):
        """
        Create a new object with the given kwargs, saving it to the database
        and returning the created object. This method is basically a `create()` wrapper,
        and saves the object as vendor data without a tenant.
        """
        return self.create(*args, **kwargs, __vendor=True)


class PackageDataModel(models.Model):
    """An abstract base model for everything that can be packaged.

    .. note:

    If a vendor update drops in, it will override rows in this dataset that match the
    `uuid` field.
    """

    class Meta:
        abstract = True

    objects = PackageDataManager()

    # The uuid field is for uniquely identifying this item across practices,
    # = simplified distribution
    # This is not unique by design, as there could be more variations/versions/tenant's
    # instances of one row with the same UUID
    uuid = models.UUIDField(default=uuid4)

    tenant = models.ForeignKey(Tenant, on_delete=models.CASCADE, blank=True, null=True)

    @classmethod
    def get_by_uuid(cls, tenant, id: UUID):
        """Retrieve object by tenant and uuid. Caches the result."""
        cache_key = f"package_data_by_uuid:{tenant.id}:{id}"
        obj = cache.get(cache_key)
        if obj is None:
            # try to fetch object with given UUID and tenant
            try:
                obj = cls.objects.get(tenant=tenant, uuid=id)
            except cls.DoesNotExist:
                # if it fails, get vendor object
                obj = cls.objects.get(tenant__isnull=True, uuid=id)

            cache.set(cache_key, obj)
        return obj


class DataPack(BaseModel):
    """A database representation of a data pack that can be downloaded from an update
    server.

    A data pack contains data in the form of :class:`PackageDataModel`s.
    This is basically what a ValueSet in FHIR means.

    A DataPack is intended to be downloaded from an update server as zip file.
    The metadata can be saved in this DataPack model, to keep a trace of it."""

    uuid = models.UUIDField(default=uuid4)
    name = models.CharField(
        max_length=100, help_text=_("Name for this data pack (computer friendly)")
    )
    title = models.CharField(
        max_length=255, help_text=_("Name for this data pack (human friendly)")
    )
    description = MarkdownField(
        max_length=255, help_text=_("Natural description, in MarkDown")
    )
    license = models.CharField(max_length=20)
    version = models.CharField(max_length=25)
    experimental = models.BooleanField(default=False)
    publisher = models.CharField(max_length=255, blank=True)
    language = models.ForeignKey("Language", on_delete=models.PROTECT)
    # TODO medux compatibility version

    #: the model in which the field should be saved
    model = models.CharField(max_length=255)
    downloaded = models.BooleanField(default=False)
    installed = models.BooleanField(default=False)
    data_file = models.FileField(upload_to="datapacks")
