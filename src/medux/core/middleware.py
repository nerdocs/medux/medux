import logging
from asgiref.sync import iscoroutinefunction, markcoroutinefunction

logger = logging.getLogger(__file__)


class DeviceMiddleware:
    """A middleware that adds the current device to the request.

    A device commonly is a computer in this context.
    Add ``medux.common.middleware.DeviceMiddleware`` to your MIDDLEWARE
    dict in settings.py.
    You can use the device in a template to your needs:

    ```django
    <span>Device: {{ request.device }}</span>
    ```

    Or in a view:

    ```python
    logger.debug(f"Request originating from device {request.device}.")
    ```
    """

    async_capable = True
    sync_capable = False

    def __init__(self, get_response):
        self.get_response = get_response
        if iscoroutinefunction(self.get_response):
            markcoroutinefunction(self)

    async def __call__(self, request):
        response = await self.get_response(request)
        # TODO: implement device fetching in middleware
        request.device = None
        # logger.debug(f"Current device: {request.site.tenantsite}.")
        return response
