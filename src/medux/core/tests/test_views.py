import pytest
from django.test import Client

LOGIN_URL = "/accounts/login/"


@pytest.mark.django_db
def test_login_page(client: Client, admin_user):
    response = client.get(
        LOGIN_URL,
    )
    assert response.status_code == 200


@pytest.mark.django_db
def test_correct_login(client: Client, admin_user):
    response = client.post(
        LOGIN_URL, data={"username": "employee_a1", "password": "password"}
    )
    assert response.status_code == 200
