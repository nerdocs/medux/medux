# Middleware


You should add some middlewares to your Django settings.py's `MIDDLEWARE`:

* [medux.employees.middleware.EmployeeMiddleware][]
* [medux.core.middleware.DeviceMiddleware][]
* [medux.common.middleware.HtmxMessageMiddleware][]

```python
MIDDLEWARE = [
    # ...
    "medux.employees.middleware.EmployeeMiddleware",
    "medux.core.middleware.DeviceMiddleware",
    "medux.common.middleware.HtmxMessageMiddleware"
]
```

Then the current device (from where you access the application) will get added to the request automatically.

::: medux.core.middleware.DeviceMiddleware
::: medux.common.middleware.HtmxMessageMiddleware
::: medux.employees.middleware.EmployeeMiddleware