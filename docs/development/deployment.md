# Deploy MedUX on a server

This is tested on Ubuntu server 22.04. It may run on Ubuntu 20.04 too, but you have to manage a Python 3.10+ installation.

### Production environment

Have a look at our `setup-medux.sh` script. If you want to use nginx/daphne/PostGreSQL/redis on an Ubuntu 22.04 LTS server, just follow these steps:

Create a virtualenv and upgrade pip:
```bash
virtualenv .venv
. .venv/bin/activate
pip install -U pip
```

Now install MedUX:

```bash
pip install medux[deploy]
```

This should install all necessary packages for you.

Now deactivate the virtualenv again (this is important, the following steps have to be done as root!):

```bash
deactivate
```

#### The setup script

There is a setup script that does the most tasks automatically. For now, it is saved in the site-packages folder, this may be improved in following versions:

```bash
.venv/lib/python-X.X/site-packages/medux/scripts/setup-medux.sh
```

This script should ask for a few things (your ``DOMAIN`` etc.) and takes care of setting up your complete environment on an Ubuntu server, including installing the necessary system packages, setting up the user, virtual python environment for MedUX and doing the Nginx config, using the following standard packages:

* Nginx
* Django + Daphne
* PostgreSQL

It tries to be as "automagical" as possible and uses sensible defaults for ``SECRET_KEY`` and the database password, creating them randomly when needed.

Have a look first at the script to see what it does.

However, if you want to use other options, like Apache, MySQL, or Daphne etc., you are free to do it in another way.

There are a few files that are created by the setup script, you can edit them to your needs at the end, for e.g. setting up SSL certificates etc.

* `/etc/nginx/sites-available/medux.conf`
* `/etc/systemd/system/medux_daphne.service`

#### Post-installation tasks

Edit the .env file in your medux directory according to your needs, especially define MEDIA_ROOT and STATIC_ROOT.

After successful install, you should initialize MedUX:

```bash
django-admin collectstatic
django-admin migrate
django-admin initialize 
```