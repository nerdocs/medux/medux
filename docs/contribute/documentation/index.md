# Contribute Documentation

As our documentation is living right beside the implementation of MedUX, you will need to setup the project locally as if you were to contribute code.


!!! warning ""
    :construction_worker: TODO add simple guide to clone and setup for documentation locally
