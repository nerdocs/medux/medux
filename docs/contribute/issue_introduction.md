# How to create an issue for MedUX

If you found a problem (which we'll be referring to as [_bugs_](https://en.wikipedia.org/wiki/Software_bug)) with MedUX, we would be happy to know about this and take care about it.  

We track our software problems mainly on Gitlab using their [issue tracker](https://gitlab.com/nerdocs/medux/medux/-/issues), which allows us to discuss problems and collaborate on solving them. Gitlab (and other similar services) call these items to track bugs **issues** - sometimes also called _tickets_ - and we'll use this term throughout the docs as well.

Currently we only support submitting issues via Gitlab directly. This implies that you will need a Gitlab.com account to submit issues (you can register [here](https://gitlab.com/users/sign_up)).  

!!! info ""
    We are currently evaluating other ways to submit issues to allow users who do not have (or want) a Gitlab account to also submit issues.  



## Opening an issue on our Gitlab issue tracker

<a href="https://gitlab.com/nerdocs/medux/medux/-/issues" class="card" style="display: flex; align-items: center"> :material-gitlab:{ .gitlab-icon .text-lg .middle } <span style="margin-left: 8px">Take me directly to the issue tracker :material-arrow-right: </span></a>


When you open our tracker, you can open a new issue by clicking the "New Issue" button on the right side of the UI, as seen below:

![Open a new issue UI](../img/contribute/issues/gl_open_issue_ui.png)


Now you'll see the issue creation form.

![Open a new issue UI](../img/contribute/issues/gl_create_issue_form.png)

As you can also see above we provide a series of templates to create new issues for the project.  
These templates are _very_ helpful for us, as we are used to having the same structure when tackling a new issue.  


!!! note ""
    Also note that there is not only templates for bugs but also features, which you are more than welcome to create as well.

After filling in the issue issues description, you can submit the issue, to which you'll be redirected after the fact.  

You do not need to worry about the `Assignee`, `Milestone`, `Due date` and `Labels` fields. That will eventually be handled by the developers.

<div style="text-align: center; margin: 20px 0" markdown>
<span style="font-size: 20px; font-weight: bold;">:tada:&emsp;Thank you for contributing MedUX!</span>
</div>

