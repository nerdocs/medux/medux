# How to contribute to MedUX

Thank you for your interest in contributing to an open source project! :heart_hands:


There is currently multiple ways to contribute to MedUX, each of them provides a simple introduction to help you get started :raised_hands:

--- 

<div class="grid cards" markdown>

-   :material-code-braces:{ .lg .middle } __Contribute Code__

    If you're interested in helping the project with code, have a look at installing and setting up MedUX locally for your development.

    [:octicons-arrow-right-24: Get started](development/index.md)

-   :fontawesome-brands-markdown:{ .lg .middle } __Contribute Documentation__

    If you like writing documentation and user guides more, check out our guide on how to contribute to documentation.

    [:octicons-arrow-right-24: Get started](documentation/index.md)

-   :material-shield-bug-outline:{ .lg .middle } __Finding bugs__

    The unsung hero of enhancing software since ages, if you found a bug, feel free to open an issue in our repository.  
    <span style="font-size: 12px; margin-bottom: 20px">There is also a simple introdocution on how to do this, if this is your first time. [:octicons-arrow-right-24: Soft Intro](/contribute/issue_introduction)</span>

    [:octicons-arrow-right-24: I caught a :bug:](https://gitlab.com/nerdocs/medux/medux/-/issues/new)

</div>
