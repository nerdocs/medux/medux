# Plugins shipped with MedUX

The following plugins are shipped with MedUX as a bundle package:

| Plugin | Description |
|--------|-------------|
| [Common](../common/README.md) | basically a base part of MedUX and MedUX-online) |
