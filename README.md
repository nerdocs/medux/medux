# Welcome to MedUX's documentation!

[![Documentation Status](https://readthedocs.org/projects/medux/badge/?version=latest)](https://medux.readthedocs.io/?badge=latest)

MedUX is an Open Source Electronic Record, in an early (currently Pre-Alpha) state.

CAVE: This project is in planning/pre-alpha state. Don't expect this to run yet. It doesn't care about django migrations. So if your database is broken because of a major update, please just delete the DB again and start from scratch. Remember, it's pre-alpha.

You can read more at the [MedUX documentation](https://medux.readthedocs.io)

Stay tuned.
