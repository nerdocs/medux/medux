[Unit]
Description=${DJANGO_USER} WebSocket Daphne daemon
After=network.target

[Service]
Type=simple
# Environment="DJANGO_SETTINGS_MODULE=medux.settings"
User=${DJANGO_USER}
Group=${DJANGO_USER}
WorkingDirectory=${DJANGO_HOME}
ExecStart=${DJANGO_HOME}/.venv/bin/daphne -b 127.0.0.1 -p 8000 ${DJANGO_USER}.asgi:application
Restart=on-failure

[Install]
WantedBy=multi-user.target